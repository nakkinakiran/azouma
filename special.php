<!-- header -->
<?php include 'includes/header.php'; ?>

<!-- body -->
<section class="azouma-special">
  <h1>AZOUMA SPECIAL</h1>
</section>

<!-- special banner -->
<section>
  <div class="special-bnr">
    <div class="box z-depth-5">
      <div class="fadeInUp animated" data-animate="fadeInUp" data-duration="2.0s" style="animation-duration: 2s; visibility: visible;">
        <p>Warm atmosphere, Friendly staff and Delicious Food including a Large Range of Vegan and Gluten Free options.</p>
      </div>
    </div>
  </div>
</section>

<section class="special-part">
  <div class="container fadeIn animated" data-animate="fadeIn" data-duration="2.0s" style="animation-duration: 2s; visibility: visible;">
    <p> Available Sunday-Thursday </p>
    <div class="row d-flex justify-content-center slideInUp animated" data-animate="slideInUp" data-duration="1.5s" style="animation-duration: 1.5s; visibility: visible;">
      <div class="col-lg-3 block">
        <div class="offer_img">
          <h5 class="grill_text">LUNCH MENU</h5>
        </div>
        <h5>STARTER</h5>
        <h6>Oven cook stuffed mushrooms served with salad, Hummus are Bread.</h6>
        <hr>
        <h5>MAIN COURSE</h5>
        <h6>Ribs, Shish Tawuk, Chicken Wings, Kefta, Escalivada (Large selection of grill Vegetables), Rice or Couscous, Traditional Sause’s (Harissa and Lebneh) </h6>
        <hr>
        <h4>MON-FRI (12pm-3pm) &pound;9.95</h4>
        <h4>SAT-SUN (12pm-4pm) &pound;10.95</h4>
        <h4>Children under 10 Half Price 10%Service</h4>
      </div>
      <div class="col-lg-3 block">
        <div class="offer_img">
          <h5>STUDENT OFFER</h5>
        </div>
        <h5>STARTER</h5>
        <h6>Oven cook stuffed mushrooms served with salad, Hummus are Bread.</h6>
        <hr>
        <h5>MAIN COURSE</h5>
        <h6>Ribs, Shish Tawuk, Chicken Wings, Kefta, Escalivada (Large selection of grill Vegetables), Rice or Couscous, Traditional Sause’s (Harissa and Lebneh) </h6>
        <hr>
        <h4>MON-FRI (12pm-3pm) &pound;7.95</h4>
        <h4>SUN-THURS (5pm-9pm)</h4>
        <h4>25% discount off the entire bill</h4>
      </div>
      <div class="col-lg-3 block">
        <div class="offer_img">
          <h5>THE GRILL NIGHT</h5>
        </div>
        <h5>STARTER</h5>
        <h6>Oven cook stuffed mushrooms served with salad, Hummus are Bread.</h6>
        <hr>
        <h5>MAIN COURSE</h5>
        <h6>Ribs, Shish Tawuk, Chicken Wings, Kefta, Escalivada (Large selection of grill Vegetables), Rice or Couscous, Traditional Sause’s (Harissa and Lebneh) </h6>
        <hr>
        <h4>Every Tuesday (5pm-9pm) Special Offer to share</h4>
        <h4>The grill Night 2 people &pound;28</h4>
        <h4>The Grill Celebration 4 people &pound;45</h4>
        <h4>Vegetarian/Vegan Grill 2 People &pound;26 / 4 People &pound;40</h4>
      </div>
    </div>
  </div>
</section>

<!-- second banner -->
<section class="container part4">
  <div class="row">
    <div class="part4-img col-gl-5" style="">
      <div class="box z-depth-5">
        <div class="fadeInUp animated" data-animate="fadeInUp" data-duration="2.0s" style="animation-duration: 2s; visibility: visible;">
          <h2>AUTHENTIC CUISINE</h2>
          <p><b>'Azouma'</b> meaning <b>'Invitation'</b> in Arabic describes the warm atmosphere and a friendly, Welcoming feeling to all of our guests. The restaurant offers a variety of dishes from all Arabia with specially of Moroccan and Lebanese cuisines.</p>
          <a href="#">About Us<i class="fas fa-chevron-right"></i></a><a href="">Our Menu<i class="fas fa-chevron-right"></i></a>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="container part5">
  <div class="row slideInUp animated" data-animate="slideInUp" data-duration="2.0s" style="animation-duration: 2s; visibility: visible;">
    <div class="col-lg-4">
      <h3>Opening Hours</h3>
      <i>Restaurant is closed on holidays.</i>
    </div>
    <div class="col-lg-3">
      <h5>Monday - Friday</h5>
      <p>12N - 3 pm</p>
      <p>5 pm - 11 pm</p>
      <p class="table_link"><a href="book-a-table.php" class="">Book a Table</a></p>
    </div>
    <div class="col-lg-3">
      <h5>Saturday</h5>
      <p>12 N- 12 am</p>
    </div>
    <div class="col-lg-2">
      <h5>Sunday</h5>
      <p>12 N - 10 pm</p>
    </div>
  </div>
</section>

<hr class="container">

<section class="container part6">
  <div class="row slideInUp animated" data-animate="slideInUp" data-duration="2.0s" style="animation-duration: 2s; visibility: visible;">
    <div class="col-lg-4">
      <h5>Reserve a Table</h5>
      <p>01227 760076</p>
    </div>
    <div class="col-lg-4">
      <h5>Enquiries</h5>
      <p>info@azouma.co.uk</p>
    </div>
    <div class="col-lg-4">
      <h5>Address</h5>
      <h6>4 Church street, St.pauls,</h6>
      <h6>canterbury, CT1 1NH</h6>
    </div>
  </div>
</section>

<!-- footer -->
<?php include 'includes/footer.php'; ?>
