<!-- header -->
<?php include 'includes/header.php'; ?>

<!-- body -->
<section class="contact-us">
  <div class="container">
    <h1>Contactus</h1>
    <div class="row d-flex justify-content-center">
      <div class="col-lg-8">
        <div class="zoomIn animated" data-animate="zoomIn" data-duration="2s" style="animation-duration: 2s; visibility: visible;">
          <p>Our staff are always ready to help and we will respond to your query as soon as possible.</p>
          <form class="" action="#" method="post">
            <div class="row">
              <div class="col-lg-6">
                <input type="text" name="" value="" placeholder="First Name" class="form-control" required>
              </div>
              <div class="col-lg-6">
                <input type="text" name="" value="" placeholder="Phone Number" class="form-control" required>
              </div>
            </div>
            <div class="">
              <input type="text" name="" value="" placeholder="Email Address" class="form-control" required>
            </div>
            <div class="">
              <textarea name="name" rows="3" cols="80" placeholder="Write your query..." class="form-control" required></textarea>
            </div>
            <button type="submit" name="button" class="btn btn-submit">Submit</button>
          </form>
        </div>
      </div>
      <!-- <div class="col-lg-5">
        <div class="zoomIn animated" data-animate="zoomIn" data-duration="2s" style="animation-duration: 2s; visibility: visible;">
          <ul>
            <li><i class="fa fa-envelope-open"></i>info@azouma.co.uk</li>
            <li><i class="fa fa-phone"></i>01227 760076</li>
            <li><i class="fa fa-map-marker"></i>4 Church street, St.pauls,
                canterbury, CT1 1NH</li>
          </ul>
        </div>
      </div> -->
    </div>
  </div>
</section>

<hr class="container">

<section class="container contact-social">
  <div class="row d-flex justify-content-center slideInUp animated" data-animate="slideInUp" data-duration="1.5s" style="animation-duration: 1.5s; visibility: visible;">
    <div class="col-lg-4 m-auto">
      <h4 class="font-weight-bold">Find us on:
      <a href=""><i class="fab fa-facebook mr-2"></i></a><a href=""><i class="fab fa-twitter mr-2"></i></a><a href=""><i class="fab fa-instagram mr-2"></i></a></h4>
    </div>
    <div class="col-lg-3 m-auto">
      <i class="fa fa-envelope-open mr-2"></i><span>info@azouma.co.uk</span>
    </div>
  <!-- </div> -->
  <!-- <div class="row d-flex justify-content-center"> -->
    <div class="col-lg-3">
      <i class="fa fa-map-marker mr-2"></i><span>4 Church street, St.pauls,canterbury, CT1 1NH</span>
    </div>
    <div class="col-lg-2 m-auto">
      <i class="fa fa-phone mr-2"></i><span>01227 760076</span>
    </div>
  </div>
</section>

<hr class="container">

<section class="container part5">
  <div class="row slideInUp animated" data-animate="slideInUp" data-duration="2.0s" style="animation-duration: 2s; visibility: visible;">
    <div class="col-lg-4">
      <h3>Opening Hours</h3>
      <i>Restaurant is closed on holidays.</i>
    </div>
    <div class="col-lg-3">
      <h5>Monday - Friday</h5>
      <p>12N - 3 pm</p>
      <p>5 pm - 11 pm</p>
    </div>
    <div class="col-lg-3">
      <h5>Saturday</h5>
      <p>12 N- 12 am</p>
    </div>
    <div class="col-lg-2">
      <h5>Sunday</h5>
      <p>12 N - 10 pm</p>
    </div>
  </div>
</section>

<!-- footer -->
<?php include 'includes/footer.php'; ?>
