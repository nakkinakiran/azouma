<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AZOUMA</title>
    <link rel="icon" type="icon/png" href="">
    <!-- Font Awesome -->
  	<link rel="stylesheet" href="node_modules/fontawesome/css/all.css">
  	<!-- Bootstrap core CSS -->
  	<link href="node_modules/mdbootstrap/css/bootstrap.min.css" rel="stylesheet">
  	<!-- Material Design Bootstrap -->
  	<link href="node_modules/mdbootstrap/css/mdb.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- owl-carousel theme -->
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="clockpicker/dist/bootstrap-clockpicker.min.css">
    <link rel="stylesheet" href="date-picker/jquery-ui.css">
    <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- custom styles -->
    <link href="css/styles.css" rel="stylesheet">
    <!-- JQuery -->
    <script type="text/javascript" src="node_modules/mdbootstrap/js/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="node_modules/mdbootstrap/js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="node_modules/mdbootstrap/js/bootstrap.min.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="clockpicker/dist/bootstrap-clockpicker.min.js"></script>
    <script src="date-picker/jquery-ui.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
    <script src="js/scrolla.jquery.min.js"></script>
  </head>
  <body>
    <header class="container-fluid">
      <div class="row">
        <div class="col-lg-5">
          <ul class=" left-menu">
            <li><a href="index.php">Home</a></li>
            <li><a href="#">Description</a></li>
            <li><a href="#">History</a></li>
            <li><a href="special.php">Gallery</a></li>
          </ul>
        </div>
        <div class="col-lg-2">
          <a href="index.php" class="brand-logo"><img src="images/azouma-logo.png" alt=""></a>
        </div>
        <div class="col-lg-5">
          <ul class="right-menu">
            <li><a href="#">Offers</a></li>
            <li><a href="menu.php">Menu</a></li>
            <li><a href="#">Job Offers</a></li>
            <li><a href="contact-us.php">Contact Us</a></li>
            <!-- <li><a href="#">Reviews</a></li> -->
            <!-- <li><a href="#">Calendar</a></li> -->
            <!-- <li><a href="book-a-table.php">Book a Table</a></li> -->
            <!-- <li>
              <nav class="nav social-links">
                <a href="#" class="nav-link"><i class="fab fa-facebook"></i></a>
                <a href="#" class="nav-link"><i class="fab fa-twitter-square"></i></a>
                <a href="#" class="nav-link"><i class="fab fa-linkedin"></i></a>
              </nav>
            </li> -->
          </ul>
        </div>
      </div>
    </header>
  </body>
</html>
