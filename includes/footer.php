<footer>
  <div class="container">
    <div class="row fadeIn animated" data-animate="fadeIn" data-duration="2.0s" style="animation-duration: 2s; visibility: visible;">
      <div class="col-lg-4 col-md-6 col-sm-12">
        <a href=""><i class="fab fa-facebook"></i></a>
        <a href=""><i class="fab fa-twitter-square"></i></a>
        <a href=""><i class="fab fa-linkedin"></i></a>
        <a href="#" class="terms">Terms</a>
        <a href="#">Privacy Policy</a>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-12">
        <p>&#169; 2019 Azouma Restaurant. All Rights Reserved</p>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-12">
        <a href="https://www.quickinnovations.co.uk/" target="_blank">Web Design London Quick Innovation</a>
      </div>
    </div>
  </div>
</footer>
<script src="js/owl.carousel.js"></script>
<script>
 $('.carousel1').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    dots:false,
    responsiveClass:true,
    autoplay:true,
    autoplayTimeout:3000,
    navText : ["<i class='fa fa-chevron-left leftthemify'></i>","<i class='fa fa-chevron-right rightthemify'></i>"],
    responsive:{
      0:{
        items:1,
      },
      600:{
        items:2,
      },
      1000:{
        items:2,
        loop:true
      }
    }
  });
</script>
<script>
  $('.animated').scrolla();
</script>
<script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
</script>
<script type="text/javascript">
$('.clockpicker').clockpicker()
  .find('input').change(function(){
    console.log(this.value);
  });
var input = $('#single-input').clockpicker({
  placement: 'bottom',
  align: 'left',
  autoclose: true,
  'default': 'now'
});
var input = $('#single-input1').clockpicker({
  placement: 'bottom',
  align: 'left',
  autoclose: true,
  'default': 'now'
});

$('.clockpicker-with-callbacks').clockpicker({
    donetext: 'Done',
    init: function() {
      console.log("colorpicker initiated");
    },
    beforeShow: function() {
      console.log("before show");
    },
    afterShow: function() {
      console.log("after show");
    },
    beforeHide: function() {
      console.log("before hide");
    },
    afterHide: function() {
      console.log("after hide");
    },
    beforeHourSelect: function() {
      console.log("before hour selected");
    },
    afterHourSelect: function() {
      console.log("after hour selected");
    },
    beforeDone: function() {
      console.log("before done");
    },
    afterDone: function() {
      console.log("after done");
    }
  })
  .find('input').change(function(){
    console.log(this.value);
  });

// Manually toggle to the minutes view
$('#check-minutes').click(function(e){
  // Have to stop propagation here
  e.stopPropagation();
  input.clockpicker('show')
      .clockpicker('toggleView', 'minutes');
});
$(document).ready(function() {
    $("label").click(function() {
      var $this = $(this);                           // store jQuery object
      $this.addClass("active");                      // add to clicked item
  });
});
</script>
<!-- <script>
  $(document).ready(function(){
    $('#courses-block').hide();
    $('#starter-btn').click(function(){
      $('#starters-block').show();
      $('#courses-block').hide();
    });
    $('#courses-btn').click(function(){
      $('#starters-block').hide();
      $('#courses-block').show();
    });
  });
</script> -->
 </body>
</html>
