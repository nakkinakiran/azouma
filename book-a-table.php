<!-- header -->
<?php include 'includes/header.php'; ?>

<!-- body -->
<section class="book-table">
  <div class="container">
    <h1>BOOK A TABLE</h1>
    <div class="row">
      <div class="col-lg-2"></div>
      <div class="col-lg-8 col-md-8 col-sm-12">
        <div class="fadeInUp animated" data-animate="fadeInUp" data-duration="2s" style="animation-duration: 2s; visibility: visible;">
          <p>Please fill out the form below and we will verify your reservation</p>
          <form class="" action="#" method="post">
            <div class="row">
              <div class="col-lg-6">
                <input type="text" name="" value="" placeholder="First Name" class="form-control" required>
              </div>
              <div class="col-lg-6">
                <input type="text" name="" value="" placeholder="Last Name" class="form-control" required>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6">
                <input type="text" name="" value="" placeholder="Phone Number" class="form-control" required>
              </div>
              <div class="col-lg-6">
                <input type="text" name="" value="" placeholder="Email Address" class="form-control" required>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4">
                <input type="text" id="datepicker" name="" value="" placeholder="Booking Date" class="form-control" required>
              </div>
              <div class="col-lg-4">
                <input type="text" id="single-input" name="time" name="" value="" placeholder="Booking Time" class="form-control" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <span class="ti-time"></span>
              </div>
              <div class="col-lg-4">
                <input type="number" name="" value="" placeholder="How Many?" class="form-control" required>
              </div>
            </div>
            <button type="submit" name="button" class="btn btn-reserve">Reserve Now</button>
          </form>
        </div>
      </div>
      <div class="col-lg-2"></div>
    </div>
  </div>
</section>

<!-- footer -->
<?php include 'includes/footer.php';
