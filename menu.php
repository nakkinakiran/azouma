<!-- header -->
<?php include 'includes/header.php'; ?>

<!-- body -->
<section class="azouma-special">
  <h1>MENU</h1>
</section>

<!-- special banner -->
<section>
  <div class="menu-bnr">
    <div class="box z-depth-5">
      <div class="fadeInUp animated" data-animate="fadeInUp" data-duration="2.0s" style="animation-duration: 2s; visibility: visible;">
        <p>Warm atmosphere, Friendly staff and Delicious Food including a Large Range of Vegan and Gluten Free options.</p>
      </div>
    </div>
  </div>
</section>

<!-- menu-block -->
<section class="menu-block">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <ul class="nav nav-tabs">
        <li><a href="#starters" data-toggle="tab" class="active">Starters</a></li>
        <li><a href="#courses" data-toggle="tab">Main Courses</a></li>
        <li><a href="#salads" data-toggle="tab">Salads</a></li>
        <li><a href="#soups" data-toggle="tab">Soups</a></li>
        <li><a href="#char-grills" data-toggle="tab">Char Grills</a></li>
        <li><a href="#side-orders" data-toggle="tab">Side Orders</a></li>
        <li><a href="#children-menu" data-toggle="tab">Children Menu</a></li>
      </ul>
    </div>
    <div class="tab-content">
      <!-- starters -->
      <div class="row tab-pane fadein active" id="starters">
        <div class="col-lg-12 block1">
          <div class="block">
            <div class="offer_img">
              <h5 class="starters_text">STARTERS</h5>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-6">
                <h5>Hummus with chicken / Hummus</h5>
                <h6>&pound;6.95 / &pound;5.30</h6>
                <p>A true favourite pureed chikpeas dip with sesame paste, seasoned with lemon juice, olive oil & garlic served with bread.</p>
              </div>
              <div class="col-lg-6">
                <h5>Baba Ghanoush (Moutabel)</h5>
                <h6>&pound;5.95</h6>
                <p>Flavoursome grilled & smoked aubergine with sesame paste. Seasoned with a touch of garlic, olive oil & lemon juice, garnished with pomegranate. Served with bread.</p>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6">
                <h5>Marinated Olives</h5>
                <h6>&pound;4.75</h6>
                <p>A true favourite pureed chikpeas dip with sesame paste, seasoned with lemon juice, olive oil & garlic served with bread.</p>
              </div>
              <div class="col-lg-6">
                <h5>Chicken Briwats</h5>
                <h6>&pound;5.95</h6>
                <p>Filo pastry parcels filled with chicken Moroccan mix of fresh herbs served with harissa and olives.</p>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6">
                <h5>Falafel</h5>
                <h6>&pound;4.75</h6>
                <p>A true favourite pureed chikpeas dip with sesame paste, seasoned with lemon juice, olive oil & garlic served with bread.</p>
              </div>
              <div class="col-lg-6">
                <h5>Taktouka king prawns</h5>
                <h6>&pound;5.95</h6>
                <p>cooked with tomato sauce, peppers, raselhanout, ginger, garlic served with bread.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- main courses -->
      <div class="row tab-pane fade" id="courses">
        <div class="col-lg-12 block1">
          <div class="block">
            <div class="offer_img">
              <h5 class="courses_text">Main Courses</h5>
            </div>
          </div>
          <div class="container">
            <div class="courses-title">
              <h4>Tagine Dishes for One or for Two</h4>
            </div>
            <div class="row">
              <div class="col-lg-6">
                <h5>Lamb Tagine with prunes</h5>
                <h6>&pound;16.95 / &pound;31.90</h6>
                <p>Slow Cooked Lamb with Prunes, caramelized pineapple &Roasted Almonds) Authentic Moroccan flavours: sweet & savoury spices. We recommend a side order with your main dish</p>
              </div>
              <div class="col-lg-6">
                <h5>Fish tagine</h5>
                <h6>&pound;17.95 / &pound;33.90</h6>
                <p>King prawn and sea bass cooked wih mixed peppers, courgetee, potatoes olives, lemon, ginger, garlic and coriander.</p>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6">
                <h5>Meat ball Tagine</h5>
                <h6>&pound;15.95 / &pound;29.90</h6>
                <p>Meat balls tagine cooked with mixed peppers, tomato sauce, coriander, ginger & (eggs) optional) Authentic Moroccan flavours. We recommend a side order with your main dish</p>
              </div>
              <div class="col-lg-6">
                <h5>Chicken Tagine</h5>
                <h6>&pound;15.95 / &pound;29.90</h6>
                <p>Chicken with mix peppers, Preserved Lemons, Olives cooked with coriander, ginger, garlic. Our slow –cooked, authentic Moroccan recipe.</p>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6">
                <h5>Vegetarian Tagine</h5>
                <h6>&pound;12.95 / &pound;23.90</h6>
                  <p>Vegetables cooked with peas, Courgettes, mixed peppers, manchgo. Aubergine, olives. coriander, and mixed spices olive oil.</p>
              </div>
              <div class="col-lg-6"></div>
            </div>
            <div class="courses-title">
              <h4>Couscous Dishes</h4>
              <p>Couscous is the celebrated dish of Morocco and is a type of delicate semolina made from Wheat. Authentic Moroccan flavours, well steamed & softened fine grains, flavoured in aromatic Moroccan spices and herbs.</p>
            </div>
            <div class="row">
              <div class="col-lg-6">
                <h5>Couscous Royal</h5>
                <h6>&pound;17.95</h6>
                <p>Couscous served with grilled chicken,lamb, merguez, and seven vegetable stew.</p>
              </div>
              <div class="col-lg-6">
                <h5>Vegetarian Couscous</h5>
                <h6>&pound;14.95</h6>
                <p>Served with grilled halloumi and seven vegetable stew.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- second banner -->
<section class="container part4">
  <div class="row">
    <div class="part4-img col-gl-5" style="">
      <div class="box z-depth-5">
        <div class="fadeInUp animated" data-animate="fadeInUp" data-duration="2.0s" style="animation-duration: 2s; visibility: visible;">
          <h2>AUTHENTIC CUISINE</h2>
          <p><b>'Azouma'</b> meaning <b>'Invitation'</b> in Arabic describes the warm atmosphere and a friendly, Welcoming feeling to all of our guests. The restaurant offers a variety of dishes from all Arabia with specially of Moroccan and Lebanese cuisines.</p>
          <a href="#">About Us<i class="fas fa-chevron-right"></i></a><a href="">Our Menu<i class="fas fa-chevron-right"></i></a>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="container part5">
  <div class="row slideInUp animated" data-animate="slideInUp" data-duration="2.0s" style="animation-duration: 2s; visibility: visible;">
    <div class="col-lg-4">
      <h3>Opening Hours</h3>
      <i>Restaurant is closed on holidays.</i>
    </div>
    <div class="col-lg-3">
      <h5>Monday - Friday</h5>
      <p>12N - 3 pm</p>
      <p>5 pm - 11 pm</p>
      <p class="table_link"><a href="book-a-table.php" class="">Book a Table</a></p>
    </div>
    <div class="col-lg-3">
      <h5>Saturday</h5>
      <p>12 N- 12 am</p>
    </div>
    <div class="col-lg-2">
      <h5>Sunday</h5>
      <p>12 N - 10 pm</p>
    </div>
  </div>
</section>

<hr class="container">

<section class="container part6">
  <div class="row slideInUp animated" data-animate="slideInUp" data-duration="2.0s" style="animation-duration: 2s; visibility: visible;">
    <div class="col-lg-4">
      <h5>Reserve a Table</h5>
      <p>01227 760076</p>
    </div>
    <div class="col-lg-4">
      <h5>Enquiries</h5>
      <p>info@azouma.co.uk</p>
    </div>
    <div class="col-lg-4">
      <h5>Address</h5>
      <h6>4 Church street, St.pauls,</h6>
      <h6>canterbury, CT1 1NH</h6>
    </div>
  </div>
</section>

<!-- footer -->
<?php include 'includes/footer.php'; ?>
